/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm.h"
#include "osdlp.h"
#include "osdlp_queue_handle.h"
#include "watchdog.h"
#include "error.h"
#include "qubik.h"

extern osMutexId osdlp_rx_mtxHandle;
extern struct watchdog hwdg;
extern struct qubik hqubik;
extern osTimerId telemetry_timerHandle;



static void
post_deploy_holdoff(uint8_t wdgid)
{
	while (hqubik.settings.holdoff_timer < QUBIK_POST_DEPLOY_HOLDOFF_TIME) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(POST_DEPLOY_HOLDOFF_TIME_STEP * 1000);
		hqubik.settings.holdoff_timer += POST_DEPLOY_HOLDOFF_TIME_STEP;
		qubik_write_settings(&hqubik);
	}
}

int
fsm_task()
{
	uint8_t wdgid = 0;
	fsm_state_t last_state = FSM_POST_DEPLOY_HOLDOFF;
	watchdog_register(&hwdg, &wdgid, "fsm");
	osDelay(4 * POWER_POLLING_INTERVAL);
	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		switch (hqubik.fsm_state) {
			case FSM_POST_DEPLOY_HOLDOFF:
#if !defined BYPASS_POST_DEPLOY_SEQUENCE || (BYPASS_POST_DEPLOY_SEQUENCE != 1)
				if (!hqubik.settings.first_deploy) {
					hqubik.fsm_state = FSM_ANTENNA_DEPLOY;
					break;
				}
				hqubik.settings.antenna_status.ant_deploy_test = ANT_DEPLOY_TEST_PENDING;
				post_deploy_holdoff(wdgid);
				hqubik.settings.first_deploy = 0;
				qubik_write_settings(&hqubik);
#endif
				hqubik.fsm_state = FSM_ANTENNA_DEPLOY;
				break;
			case FSM_ANTENNA_DEPLOY:
#if !defined BYPASS_POST_DEPLOY_SEQUENCE || (BYPASS_POST_DEPLOY_SEQUENCE != 1)
				if (!hqubik.settings.antenna_first_deploy) {
					hqubik.fsm_state = FSM_TC_MODE;
					break;
				}
				if (hqubik.status.power_save == 1) {
					break;
				}
				if (hqubik.settings.antenna_status.antenna_retry_count <
				    QUBIK_ANTENNA_RETRY_LIMIT) {
					// Deploy test
					hqubik.settings.antenna_status.ant_deploy_test = antenna_deploy_test();
					if (hqubik.settings.antenna_status.ant_deploy_test == ANT_DEPLOY_TEST_FAIL) {
						hqubik.settings.antenna_status.ant_deploy_test = antenna_deploy_test();
					}
					// Deploy antenna
					hqubik.settings.antenna_status.ant_deploy_status = antenna_deploy(&hwdg, wdgid);
					hqubik.settings.antenna_pwr_stats.ant_deploy_avg_current =
					        hqubik.power.current_avg;
					hqubik.settings.antenna_pwr_stats.ant_deploy_avg_voltage =
					        hqubik.power.voltage_avg;
				}
				hqubik.settings.antenna_first_deploy = 0;
				qubik_write_settings(&hqubik);

#endif
				hqubik.fsm_state = FSM_TC_MODE;
				break;
			case FSM_TC_MODE:
				if (hqubik.status.power_save) {
					hqubik.fsm_state = FSM_LOW_POWER_MODE;
					break;
				}
				if (last_state != FSM_TC_MODE) {
					osTimerStart(telemetry_timerHandle, QUBIK_TELEMETRY_INTERVAL);
					last_state = FSM_TC_MODE;
				}
				/*
				 * if there is a request for experiment, reset
				 * this request and go to FSM_EXPERIMENT_MODE
				 */
				if (hqubik.experiment_req) {
					hqubik.experiment_req = 0;
					hqubik.fsm_state = FSM_EXPERIMENT_MODE;
				}
				break;
			case FSM_EXPERIMENT_MODE:
				if (last_state != FSM_EXPERIMENT_MODE) {
					last_state = FSM_EXPERIMENT_MODE;
				}
				if (hqubik.status.power_save) {
					hqubik.experiment_stop = 1;
					hqubik.fsm_state = FSM_LOW_POWER_MODE;
					break;
				}
				if (hqubik.experiment_completed) {
					hqubik.fsm_state = FSM_TC_MODE;
					break;
				}
				// reset beacon timer
				osTimerStart(telemetry_timerHandle, QUBIK_TELEMETRY_INTERVAL);
				break;
			case FSM_LOW_POWER_MODE:
				hqubik.experiment_stop = 1;
				if (!hqubik.status.power_save) {
					hqubik.fsm_state = FSM_TC_MODE;
					break;
				}
				if (last_state != FSM_LOW_POWER_MODE) {
					osTimerStart(telemetry_timerHandle, QUBIK_TELEMETRY_INTERVAL_LP);
					last_state = FSM_LOW_POWER_MODE;
				}
				break;
			default:
				break;
		}
		osDelay(100);
	}
}
