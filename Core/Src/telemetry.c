/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  @file telemetry.c
 *
 *  @date Jul 24, 2020
 *  @brief Telemetry data functions
 */

#include <telemetry.h>
#include <error.h>
#include <osdlp.h>
#include <osdlp_queue_handle.h>
#include <string.h>
#include <manifesto.h>

extern osMutexId osdlp_tx_mtxHandle;
static struct tx_frame tmp_frame;
uint8_t tm_proxy[OSDLP_MAX_TM_PACKET_LENGTH];

static int8_t tx_power_lut[16] =
{ -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10, 11, 12, 13, 14, 15 };

static int
parse_telemetry_entry(struct telemetry_map_entry *e, uint16_t x)
{
	/* Baudrate */
	uint8_t f = x & 0x7;
	switch (f) {
		case RADIO_BAUD_600:
		case RADIO_BAUD_1200:
		case RADIO_BAUD_2400:
		case RADIO_BAUD_4800:
		case RADIO_BAUD_9600:
		case RADIO_BAUD_19200:
		case RADIO_BAUD_38400:
		case RADIO_BAUD_125000:
			e->meta.baud = (radio_baud_t) f;
			break;
		default:
			return -INVAL_PARAM;
	}
	/* Encoding */
	f = (x >> 3) & 0x3;
	switch (f) {
		case RADIO_ENC_RAW:
		case RADIO_ENC_RS:
		case RADIO_ENC_CC_1_2_RS:
			e->meta.enc = (radio_encoding_t) f;
			break;
		default:
			return -INVAL_PARAM;
	}
	/* Modulation */
	f = (x >> 5) & 0x7;
	switch (f) {
		case RADIO_MOD_FSK:
		case RADIO_MOD_BPSK:
		case RADIO_MOD_BPSK_RES:
		case RADIO_MOD_QPSK:
		case RADIO_MOD_RILDOS:
			e->meta.mod = (radio_modulation_t) f;
			break;
		default:
			return -INVAL_PARAM;
	}

	/* Telemetry type */
	f = (x >> 8) & 0x7;
	switch (f) {
		case TELEMETRY_BASIC:
		case TELEMETRY_NOP:
		case TELEMETRY_SKIP:
		case TELEMETRY_MANIFESTO:
			e->type = (telemetry_t) f;
			break;
		default:
			return -INVAL_PARAM;
	}

	/* TX power */
	f = (x >> 11) & 0xf;
	e->meta.tx_power = tx_power_lut[f];

	/* Enable PA */
	e->meta.enable_pa = (x >> 15) & 0x1;
	return NO_ERROR;
}

int
transmit_tm(const uint8_t *pkt, uint16_t length, uint8_t vcid,
            const struct tx_frame_metadata *meta)
{
	if (pkt == NULL || meta == NULL) {
		return -OSDLP_NULL;
	}
	struct tm_transfer_frame *tm;
	struct tc_transfer_frame *tc;
	int ret = 0;
	if (osMutexWait(osdlp_tx_mtxHandle, 200) == osOK) {
		ret = tm_get_tx_config(&tm, vcid);
		if (ret) {
			osMutexRelease(osdlp_tx_mtxHandle);
			return ret;
		}
		ret = osdlp_tc_get_rx_config(&tc, vcid);
		if (ret) {
			osMutexRelease(osdlp_tx_mtxHandle);
			return ret;
		}
		/* Copy length of TM in first two bytes */
		tm_proxy[0] = (length >> 8) & 0xff;
		tm_proxy[1] = length & 0xff;

		memcpy(&tm_proxy[2], pkt, length * sizeof(uint8_t));
		osdlp_prepare_clcw(tc, tm->ocf);
		register_meta(meta);
		ret = osdlp_tm_transmit(tm, tm_proxy, length + sizeof(uint16_t));
		osMutexRelease(osdlp_tx_mtxHandle);
		return ret;
	} else {
		return -OSDLP_MTX_LOCK;
	}
}

size_t
telemetry_basic_frame(const struct qubik *q, uint8_t *buffer)
{
	size_t cntr = 0;

	buffer[cntr++] = (q->power.current >> 8) & 0xff;
	buffer[cntr++] = (q->power.current & 0xff);
	buffer[cntr++] = (q->power.voltage >> 8) & 0xff;
	buffer[cntr++] = (q->power.voltage) & 0xff;
	buffer[cntr++] = (q->power.temperature_die) & 0xff;
	buffer[cntr++] = (q->power.temperature) & 0xff;
	buffer[cntr++] = (q->power.current_min >> 8) & 0xff;
	buffer[cntr++] = (q->power.current_min) & 0xff;
	buffer[cntr++] = (q->power.voltage_min >> 8) & 0xff;
	buffer[cntr++] = (q->power.voltage_min) & 0xff;
	buffer[cntr++] = (q->power.temperature_min) & 0xff;
	buffer[cntr++] = (q->power.current_max >> 8) & 0xff;
	buffer[cntr++] = (q->power.current_max) & 0xff;
	buffer[cntr++] = (q->power.voltage_max >> 8) & 0xff;
	buffer[cntr++] = (q->power.voltage_max) & 0xff;
	buffer[cntr++] = (q->power.temperature_max) & 0xff;
	buffer[cntr++] = (q->power.current_avg >> 8) & 0xff;
	buffer[cntr++] = (q->power.current_avg) & 0xff;
	buffer[cntr++] = (q->power.voltage_avg >> 8) & 0xff;
	buffer[cntr++] = (q->power.voltage_avg) & 0xff;
	buffer[cntr++] = (q->power.temperature_avg) & 0xff;
	buffer[cntr++] = (q->power.TTE >> 8) & 0xff;
	buffer[cntr++] = (q->power.TTE) & 0xff;
	buffer[cntr++] = (q->power.RepCap >> 8) & 0xff;
	buffer[cntr++] = (q->power.RepCap) & 0xff;
	buffer[cntr++] = (q->power.FullRepCap >> 8) & 0xff;
	buffer[cntr++] = (q->power.FullRepCap) & 0xff;
	buffer[cntr++] = (q->power.SOC) & 0xff;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_test_voltage  >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_test_voltage) & 0xff;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_test_current  >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_test_current) & 0xff;
	buffer[cntr++] = (q->settings.antenna_status.ant_deploy_test) & 0x03;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_avg_voltage >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_avg_voltage) & 0xff;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_avg_current >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.antenna_pwr_stats.ant_deploy_avg_current) & 0xff;
	buffer[cntr++] = (q->settings.antenna_status.antenna_deploy_time) & 0xff;
	buffer[cntr++] = (q->settings.antenna_status.ant_deploy_status) & 0x03;
	buffer[cntr++] = (q->settings.antenna_status.antenna_retry_count) & 0xff;
	buffer[cntr++] = (q->uptime_secs >> 24) & 0xff;
	buffer[cntr++] = (q->uptime_secs >> 16) & 0xff;
	buffer[cntr++] = (q->uptime_secs >> 8) & 0xff;
	buffer[cntr++] = (q->uptime_secs) & 0xff;
	buffer[cntr++] = (q->power.temperature_mcu) & 0xff;
	buffer[cntr++] = (q->power.rtc_vbat >> 8) & 0xff;
	buffer[cntr++] = (q->power.rtc_vbat) & 0xff;
	buffer[cntr++] = (q->settings.first_deploy) & 0x01;
	buffer[cntr++] = (q->settings.antenna_first_deploy) & 0x01;
	buffer[cntr++] = (q->reason_of_reset) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames_invalid >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames_invalid) & 0xff;
	buffer[cntr++] = (q->settings.reset_counters.low_power_counter >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.low_power_counter) & 0xff;
	buffer[cntr++] = (q->settings.reset_counters.independent_watchdog_counter >> 8)
	                 &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.independent_watchdog_counter) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.software_counter >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.software_counter) & 0xff;
	buffer[cntr++] = (q->settings.reset_counters.brownout_counter >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.brownout_counter) & 0xff;
	buffer[cntr++] = (q->status.power_save) & 0x01;
	buffer[cntr++] = (q->status.power_mon_fail) & 0x01;

	struct tc_transfer_frame *tc;
	uint8_t clcw[4];
	int ret;
	for (int i = 0 ; i < OSDLP_TC_VCS ; i++) {
		ret = osdlp_tc_get_rx_config(&tc, i);
		if (ret) {
			buffer[cntr++] = 0xe0;
			buffer[cntr++] = 0xe0;
			buffer[cntr++] = 0xe0;
			buffer[cntr++] = 0xe0;
		} else {
			osdlp_prepare_clcw(tc, clcw);
			buffer[cntr++] = clcw[0];
			buffer[cntr++] = clcw[1];
			buffer[cntr++] = clcw[2];
			buffer[cntr++] = clcw[3];
		}
	}
	buffer[cntr++] = (q->hradio.tx_frames >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.tx_frames) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits >> 24) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits >> 16) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits) & 0xff;
	buffer[cntr++] = ((int)q->hradio.hax5043.rx_status.rssi) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.freq_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.freq_tracking) & 0xff;
	buffer[cntr++] = ((int)q->hradio.hax5043.rx_status.agc) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking >> 24) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking >> 16) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.phase_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.phase_tracking) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking >> 24) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking >> 16) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking) & 0xff;
	return cntr;
}

int
retrieve_meta(struct qubik *q, struct tx_frame_metadata *meta)
{
	if (!meta) {
		return -1;
	}
	meta->baud = q->settings.tm_resp_baud;
	meta->enc = q->settings.tm_resp_enc;
	meta->mod = q->settings.tm_resp_mod;
	meta->tx_power = q->settings.tm_resp_tx_power;
	meta->enable_pa = 1;
	return 0;
}
/**
 * Returns the index of the closest TX power of the TX power lookup table
 * @note This index should be used only for the telemetry mapping
 * @param tx_power the TX power of the AX5043 in dB in the range [-10, 15]
 * @return the index of the corresponding TX power
 */
uint8_t
telemetry_tx_power_index(int8_t tx_power)
{
	for (uint8_t i = 0; i < 16; i++) {
		if (tx_power <= tx_power_lut[i]) {
			return i;
		}
	}
	return 15;
}

void
telemetry_tx_callback(struct qubik *q)
{
	const uint16_t *map = q->settings.telemetry_map;
	struct telemetry_map_entry t;

	/*
	 * Try to iterate the map until the first valid entry is found. To
	 * avoid an infinite loop in case all of the entries are invalid or
	 * of type TELEMETRY_SKIP, the callback returns when the end of the map
	 * is reached and will try again at the next activation
	 */
	while (1) {
		int ret = parse_telemetry_entry(&t, map[q->telemetry_idx]);
		if (ret) {
			q->telemetry_idx = (q->telemetry_idx + 1) % QUBIK_TELEMETRY_MAP_LEN;
			if (q->telemetry_idx == 0) {
				return;
			}
			continue;
		}

		size_t len = 0;
		switch (t.type) {
			case TELEMETRY_BASIC:
				tmp_frame.meta = t.meta;
				len = telemetry_basic_frame(q, tmp_frame.pdu);
				transmit_tm(tmp_frame.pdu, len, VCID_REG_TM,
				            &tmp_frame.meta);
				break;
			case TELEMETRY_NOP:
				break;
			case TELEMETRY_SKIP:
				/*
				 * Skip entries, can be quite handy to efficiently
				 * support smaller mappings without too much
				 * effort
				 */
				q->telemetry_idx = (q->telemetry_idx + 1) % QUBIK_TELEMETRY_MAP_LEN;
				/* Break to avoid infinite loops */
				if (q->telemetry_idx == 0) {
					return;
				}
				continue;
			case TELEMETRY_MANIFESTO:
				tmp_frame.meta = t.meta;
				transmit_tm((uint8_t *) manifesto, sizeof(manifesto),
				            VCID_REG_TM, &tmp_frame.meta);
				break;
		}
		q->telemetry_idx = (q->telemetry_idx + 1) % QUBIK_TELEMETRY_MAP_LEN;
		return;
	}
}
